import React, { Fragment } from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import Home from "pages/Home";
import CarouselKao from "./pages/CarouselKao";
import Plant from "pages/Plant";
import Plants from "pages/Plants";
import Taxonomy from "pages/Taxonomy";
import NotFound from "pages/NotFound";
import MyNavbar from "components/MyNavbar";
import Footer from "components/Footer";

import { withAuthProvider } from 'contexts/AuthContext'

const App = withAuthProvider(() => {

  return (
    <Fragment>
      <MyNavbar />
      <main>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route exact path="/plants" component={Plants} />
          <Route exact path="/test" component={CarouselKao} />
          <Route path="/plants/:plant_id" component={Plant} />
          {/* 順序越前面的會越先判定 */}
          <Route path="/taxonomy/:taxonomy_name" component={Taxonomy} />
          <Route path="/not-found" component={NotFound} />
          <Redirect to="/not-found" />
        </Switch>
      </main>
      <Footer />
    </Fragment>
  );
});

export default App;
