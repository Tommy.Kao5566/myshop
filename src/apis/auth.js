export const postAuthLoginAPI= () => {
    
    return `/oauth2/token`;
}

export const getAuthAccountAPI= () => {
    
    return `/accounts`;
}