import React from 'react';
import SlideShow from 'components/SlideShow';
import { Environment, Mental, Physical } from 'images/BenifitsIcons';

const items = [
  { src: 'https://i.picsum.photos/id/530/1600/1440.jpg' },
  { src: 'https://i.picsum.photos/id/400/1600/1440.jpg' },
  {
    src:
      'https://images.unsplash.com/photo-1520690214124-2405c5217036?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80'
  },
  {
    src:
      'https://images.unsplash.com/photo-1526948748350-c719c5d9d015?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80'
  }
  // "https://i.picsum.photos/id/239/1600/1440.jpg",
];

const contents = [
  {
    icon: <Physical height='48px' color='#28a745' />,
    title: 'Physical Health',
    text:
      'Plants have phytoncides and airborne chemicals. These chemicals help to reduce the amount of stress you feel throughout the day.'
  },
  {
    icon: <Mental height='50px' color='#28a745' />,
    title: 'Mental Health',
    text:
      "Researchers in the United Kingdom found that people who live around nature feel much happier than those that don't."
  },
  {
    icon: <Environment height='50px' color='#28a745' />,
    title: 'Environment',
    text:
      'Research has shown that plants will remove around 87% of the pollutants in the air, some of them being the most harmful chemicals.'
  }
];

const Home = () => {
  return (
    <>
      <SlideShow items={items} />
      <div className='container text-center' style={{ margin: '10vh auto'}}>
        <h3 className='text-uppercase mt-5' style={{ letterSpacing: '3px' }}>
          Benefits of Plants
        </h3>
        <div className='row d-flex align-items-center justify-content-around'>
          {contents.map((content, index) => {
            return (
              <div key={index} className='col-md-3 col-11 pt-5'>
                {content.icon}
                <div className='my-3 font-weight-bold text-success text-uppercase'>{content.title}</div>
                <p className='font-weight-light'>{content.text}</p>
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
};

export default Home;
