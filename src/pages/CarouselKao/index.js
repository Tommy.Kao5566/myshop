import React, { useState, useRef } from "react";
import { toPrevious, toNext } from "images/PageIcons";

const CarouselKao = ({ time=1 , carouselH=600 }) => {
  
  const images = [
    "https://i.picsum.photos/id/239/1024/720.jpg",
    "https://i.picsum.photos/id/530/1024/720.jpg",
    "https://i.picsum.photos/id/400/1024/720.jpg",
    // "https://images.unsplash.com/photo-1520690214124-2405c5217036?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80",
    // "https://images.unsplash.com/photo-1526948748350-c719c5d9d015?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80",
  ]
  const lens = images.length;
  const imgWidthInPx = "100vw"
  const defaultWidthArr = images.map(()=>{
    return imgWidthInPx;
  })

  const [orders, setOrders] = useState(images.map((ele,idx)=>{
    return idx;
  }));

  const edgeValue = useRef({min:0, max:lens-1});

  const [widths, setWidths] = useState(defaultWidthArr);
  const [aniTime,setAniTime] = useState(0);
  const [btnLock,setBtnLock] = useState(false)

  const handlePrevious =()=>{
    if(btnLock){return;}
    setBtnLock(true);
    const first = orders.indexOf(edgeValue.current.min)
    setAniTime(time);
    setWidths(widths.map((ele,idx)=>{
      return idx===first?0:imgWidthInPx;
    }));
    setTimeout(()=>{
      setOrders(orders.map((ele,idx)=>{
        return idx===first?ele+lens:ele;
      }));
      setAniTime(0);
      setWidths(defaultWidthArr);
      setBtnLock(false);
    },1000*time);
    edgeValue.current.min++;
    edgeValue.current.max++;
  }

  const handleNext =()=>{
    if(btnLock){return;}
    setBtnLock(true);
    const last = orders.indexOf(edgeValue.current.max)
    setAniTime(0);
    setWidths(widths.map((ele,idx)=>{
      return idx===last?0:imgWidthInPx;
    }));
    setOrders(orders.map((ele,idx)=>{
      return idx===last?ele-lens:ele;
    }));
    setTimeout(()=>{
      setAniTime(time);
      setWidths(defaultWidthArr);
    },100);
    setTimeout(()=>{
      setBtnLock(false);
    },1000*time);
    edgeValue.current.min--;
    edgeValue.current.max--;
  }

  return (
    <div className="" style={{ 
        width:`${lens*100}vw`, 
        }}>
      <div className="d-flex" style={{ 
        marginLeft:`-${imgWidthInPx}`
        }}>
        {images.map((image, idx)=>{
          return <div 
            style={{
              backgroundImage:`url(${image})`,
              order:orders[idx],
              width:widths[idx],
              height:carouselH+'px',
              transition:`width ${aniTime}s`,
              backgroundSize: 'cover',
              backgroundRepeat: 'no-repeat',
              backgroundPosition: 'center',
            }}>
          </div>
        })}
      </div>
      <div className="d-flex justify-content-between position-absolute w-100" style={{ top:carouselH/2+'px'}}>
        <div style={{zIndex:10, cursor:'pointer', padding:'10px'}} onClick={()=>{handlePrevious()}}>
          <svg width="40" height="40" viewBox={toPrevious.viewBox} fill="#c1c4c7">
            {toPrevious.icon}
          </svg>
        </div>
        <div style={{zIndex:10, cursor:'pointer', padding:'10px'}} onClick={()=>{handleNext()}}>
          <svg width="40" height="40" viewBox={toNext.viewBox} fill="#c1c4c7">
            {toNext.icon}
          </svg>
        </div>
      </div>
    </div>
  );
};

export default CarouselKao;
