import React, { useState, useEffect } from "react";
import axios from "axios";
import { apiEndpoint, plants_token } from "apis/trefle";
import { Spinner } from "react-bootstrap";
import PlantInfo from "components/PlantInformation";
import CenterItem from "components/CenterItem";
import NoData from "components/NoData";
import Gallery from 'components/Gallery';

const PlantForModal = ({ plant_id }) => {
  const [plant, setPlant] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const source = axios.CancelToken.source();
    window.scrollTo(0, 0);
    async function getPlant() {
      setLoading(true);
      setPlant(null);
      try {
        const response = await axios.get(`${apiEndpoint}/api/plants/${plant_id}?token=${plants_token}`,
        { cancelToken: source.token }
        )
        if(response.status===200 && response.statusText==='OK'){
          setPlant(response.data);
          setLoading(false);
        }
      } catch (error) {
        console.log('error')
      }
    }
    getPlant();

    return () => {
      source.cancel();
    };
  }, [plant_id]);

  return (
    <>
      {plant ? (
        <div className="py-2 mx-4 mx-sm-5">
          <div className="row">
            <div className="p-2 col-12 col-sm-12 col-lg-6">
              <PlantInfo plant={plant} />
            </div>
            <div className="p-2 col-12 col-sm-12 col-lg-6 order-1">
              {plant?.images?.length > 0 && (
                <Gallery images={plant.images.filter(img => !img.url.endsWith('svg'))} />
              )}
            </div>
          </div>
        </div>
      )
      :
        <div className="container" style={{minHeight:'420px',padding:'40px 0 0 0'}}>
            {
            loading ? 
          <CenterItem>
              <Spinner animation="grow" /> 
          </CenterItem>
              :
              <NoData />}
        </div>
      }
    </>
  );
};

export default PlantForModal;
