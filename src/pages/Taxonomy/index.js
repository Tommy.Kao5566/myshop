import React, { Fragment, useEffect, useState } from "react";
import { apiEndpoint, plants_token } from "apis/trefle";
import axios from "axios";
import queryString from "query-string";
import Jumbotron from "components/Jumbotron";
import ResultCard from "components/ResultCard";
import Pagination from "components/Pagination";
import Spinner from "react-bootstrap/Spinner";

const Taxonomy = ({ location, history, match }) => {
  const [plants, setPlants] = useState(null);
  const [loading, setLoading] = useState(false);
  const [totalPage, setTotalPage] = useState(null);
  const [page, setPage] = useState(
    parseInt(queryString.parse(location.search).page_no)
  );
  const { taxonomy_name } = match.params;

  useEffect(() => {
    const source = axios.CancelToken.source();
    const getPlantsList = (page_no = 1) => {
      setLoading(true);
      setPlants(null);
      setPage(page_no);
  
      const data = {
        page_size: 9,
        page: page_no
      };
  
      axios
        .all([
          axios.get(
            `${apiEndpoint}/api/${taxonomy_name}?token=${plants_token}&page_size=${data.page_size}&page=${data.page}`,
            { cancelToken: source.token }
          )
        ])
        .then(
          axios.spread(plants => {
            setPlants(plants.data);
            setLoading(false);
            setTotalPage(parseInt(plants.headers['total-pages']));
            // console.log(plants.data.map((row)=>{
            //   return "'"+row.name.replace(/family/i, '')+"'";
            // }).toString());
          })
        )
        .catch(err => {
          console.error(err);
        });
    };

    window.scrollTo(0, 0);
    getPlantsList(parseInt(queryString.parse(location.search).page_no));

    return () => {
      source.cancel();
    };
  }, [location, taxonomy_name]);
  return (
    <Fragment>
      <Jumbotron
        title={taxonomy_name}
        images={`https://i.picsum.photos/id/798/4592/3448.jpg?hmac=a-NblRRC-lhb5GShHTdTomW3vlf5HZKM_aRjWQaOmNg`}
      />
      <div className="container">
        <div className="d-flex justify-content-center my-5">
          <Pagination page={page} lastPage={totalPage} setPage={setPage} history={history} location={location} firstLastBtn={true}/>
        </div>
        {loading && (
          <div className="d-flex justify-content-center my-5 py-5">
            <Spinner animation="grow" />
          </div>
        )}
        {plants &&
        <>
          <div className={"row"}>
            {plants.map((plant, index) => {
                return <ResultCard plant={plant} key={index} taxonomy_name={taxonomy_name} />;
              })}
          </div>
          <div className="d-flex justify-content-center my-5">
            <Pagination page={page} lastPage={totalPage} setPage={setPage} history={history} location={location} firstLastBtn={true}/>
          </div>
        </>
        }
      </div>
    </Fragment>
  );
};

export default Taxonomy;
