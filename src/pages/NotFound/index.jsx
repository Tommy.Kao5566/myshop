import React from "react";
import post from "images/post.svg";

const NotFound = () => {
  return (
    <div className="py-5" style={{ textAlign: "center" }}>
      <img src={post} alt="" width="50" />
      <p className="mt-3">Oops! Page Not Found</p>
    </div>
  );
};

export default NotFound;
