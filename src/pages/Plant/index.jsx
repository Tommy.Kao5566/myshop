import React, { useState, useEffect } from "react";
import axios from "axios";
import { apiEndpoint, plants_token } from "apis/trefle";
import { Link } from "react-router-dom";
import { Spinner } from "react-bootstrap";
import Jumbotron from "components/Jumbotron";
import PlantInfo from "components/PlantInformation";
import CenterItem from "components/CenterItem";
import NoData from "components/NoData";
import PlantSwitch from "components/PlantSwitch";
import Gallery from 'components/Gallery';
 
const Plant = ({ match, location }) => {

  const plant_id = match.params.plant_id;
  const [plant, setPlant] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {

    // console.log(location.data)
    window.scrollTo(0, 0);
    const source = axios.CancelToken.source();
    async function getPlant() {
      setLoading(true);
      setPlant(null);
      try {
        const response = await axios.get(`${apiEndpoint}/api/plants/${plant_id}?token=${plants_token}`,
        { cancelToken: source.token }
        )
        if(response.status===200 && response.statusText==='OK'){
          setPlant(response.data);
          setLoading(false);
        }
      } catch (error) {
        console.log('error')
      }
    }
    getPlant();
 
    return () => {
      source.cancel();
    };
  }, [plant_id]);
 
  return (
    <div className="min-vh-100">
      <Jumbotron
        title={plant?.common_name || plant?.scientific_name}
        subtitle = {
          <>
            {plant?.common_name && plant?.scientific_name + "  |  "}
            {plant?.family_common_name?
            <Link to={`/plants?family_common_name=${plant?.family_common_name}&page_no=${location?.data?.page||1}`}>{plant?.family_common_name}</Link>
            :
            <Link to={`/plants?family_name=${plant?.family_name}&page_no=${location?.data?.page||1}`}>{plant?.family_name}</Link>}
          </>
        }
      />
      {location.data &&
        <PlantSwitch 
          location={location} 
        />
      }
      {plant ? (
        <>
          <div className="py-2 mx-4 mx-sm-5">
            <div className="row">
              <div className="p-2 col-12 col-sm-12 col-md-6">
                <PlantInfo plant={plant} />
              </div>
              {plant?.images?.length > 0 &&
                <div className="p-2 col-12 col-sm-12 col-md-6">
                  <Gallery images={plant.images.filter(img => !img.url.endsWith('svg'))} />
                </div>
              }
            </div>
          </div>
        </>
      )
      :
        <div className="container" style={{minHeight:'420px',padding:'40px 0 0 0'}}>
          {
            loading ?
          <CenterItem>
              <Spinner animation="grow" />
          </CenterItem>
              :
          <NoData />}
        </div>
      }
    </div>
  );
};
 
export default Plant;