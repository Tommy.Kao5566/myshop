import React, { Fragment, useEffect, useState } from "react";
import axios from "axios";
import queryString from "query-string";
import { Spinner } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { apiEndpoint, plants_token, families_common_name_list, families_name_list } from "apis/trefle";
import Jumbotron from "components/Jumbotron";
import Card from "components/Card";
import Modal from "components/Modal";
import Pagination from "components/Pagination";
import NoData from "components/NoData";
import Explore from 'components/Explore'
import PlantForModal from "../PlantForModal";

const NameSearcher = ({ location, history, setQS, className, qs }) => {
  const [inputText, setInputText] = useState(qs||"");

  const searchByName = (qs) => {
    let searchParams = new URLSearchParams(location.search);
    searchParams.set("q", qs);
    searchParams.set("page_no", 1);
    history.push({
      pathname: location.pathname,
      search: searchParams.toString()
    });
    setInputText(qs);
    setQS(qs);
  };

  const inputCheck = (value) => {
    if (value === " ") {
      value = "";
    }
    setInputText(value);
  };

  const handleKeyPress = (e) => {
    if (e.key === 'Enter') { 
      searchByName(inputText);
    }
  }

  useEffect(()=>{
    setInputText(qs);
  },[qs]);

  return (
    <div className={className}>
      <input
        className="form-control"
        type="text"
        value={inputText}
        placeholder="Search by name..."
        onChange={e => {inputCheck(e.target.value);}}
        onKeyPress={e => {handleKeyPress(e)}}
      />
      <div className="text-muted clearIcon pointerIcon"
          onClick={() => {
            searchByName("");
          }}>
        <FontAwesomeIcon icon={faTimes} />
      </div>    
    </div>
  );
};

const DropDownFilter = ({ location, history, setFamily, className, family}) => {
  const [newItemList, setNewItemList] = useState(["Honeysuckle family","Caprifoliaceae",'Mallow family',"Malvaceae"]);
  const [inputText, setInputText] = useState(family||"");
  let searchParams = new URLSearchParams(location.search);
  const [showClass, setShowClass] = useState(false);

  const familyfilter = (inputText) => {
    const res = [];
    if (inputText.length === 1) {
      for(let i=0; i<families_name_list.length; i++){
        if(families_common_name_list[i]&&families_common_name_list[i][0].toLocaleLowerCase() === inputText.toLocaleLowerCase()){
          res[res.length] = families_common_name_list[i];
        }
        if(res.length >= 4){
          break;
        }
        if(families_name_list[i][0].toLocaleLowerCase() === inputText.toLocaleLowerCase()){
          res[res.length] = families_name_list[i];
        }
        if(res.length >= 4){
          break;
        }
      }
    } else {
      for(let i=0; i<families_name_list.length; i++){
        if(families_common_name_list[i]&&families_common_name_list[i].toLocaleLowerCase().indexOf(inputText.toLocaleLowerCase()) > -1){
          res[res.length] = families_common_name_list[i];
        }
        if(res.length >= 4){
          break;
        }
        if(families_name_list[i]&&families_name_list[i].toLocaleLowerCase().indexOf(inputText.toLocaleLowerCase()) > -1){
          res[res.length] = families_name_list[i];
        }
        if(res.length >= 4){
          break;
        }
      }
    }
    setNewItemList(res);
  };

  const searchByFamilyName = (family_whatever_name) => {
    searchParams.set('family_common_name','');
    searchParams.set('family_name','');
    if(family_whatever_name.includes("family")){
      searchParams.set("family_common_name", family_whatever_name);
    }else{
      searchParams.set("family_name", family_whatever_name);
    }
    searchParams.set("page_no", 1);
    history.push({
      pathname: location.pathname,
      search: searchParams.toString()
    });
    setInputText(family_whatever_name);
    setFamily(family_whatever_name);
  };

  const inputCheck = (value) => {
    if (value === " ") {
      value = "";
    }
    familyfilter(value);
    setInputText(value);
  };

  useEffect(()=>{
    setInputText(family);
  },[family]);

  return (
    <div className={className}>
      <input className="form-control mb-1" type="text" placeholder="Filter by family name..."
        value={inputText}
        onChange={(e) => {inputCheck(e.target.value);}}
        onFocus={()=>{
          setShowClass(true);
        }}
        onBlur={()=>{
          setShowClass(false);
        }}
      />
      <div className="text-muted clearIcon pointerIcon"
        onClick={() => {
            searchByFamilyName("");
            history.push({
            pathname: location.pathname,
            search: searchParams.toString()
          });
        }}
      >
        <FontAwesomeIcon icon={faTimes}/>
      </div>
      <ul className={`list-group dropdown ${showClass?'show':null}`} 
        style={{zIndex: '100', position: 'relative'}}>
        {newItemList
            .map((item, index) => {
              return (
                <li
                  key={"itemList_" + index}
                  className="list-group-item list-group-item-action pointer"
                  role="button"
                  onMouseDown={e => {
                    searchByFamilyName(e.target.innerText);
                    setShowClass(false);
                  }}
                >
                  {item}
                </li>
              )
            }
          )
        }
      </ul>  
    </div>
  );
}

const Plants = ({ location, history }) => {

  const [plants, setPlants] = useState(null);
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(parseInt(queryString.parse(location.search).page_no));
  const [family, setFamily] = useState(queryString.parse(location.search).family_common_name|| queryString.parse(location.search).family_name||"");
  const [qs, setQS] = useState(queryString.parse(location.search).q||"");
  const [totalPage, setTotalPage] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [indexNow, setIndexNow] = useState(null);
  const pageSize = 9;

  useEffect(() => {
    window.scrollTo(0, 0);

    const source = axios.CancelToken.source();

    const getPlantsList = (page_no) => {
      setLoading(true);
      setPlants(null);
      setPage(page_no);
      
      const data = {
        page_size: pageSize,
        page: page_no,
        family: family,
        q: qs
      };
  
      axios.get(
        `${apiEndpoint}/api/plants?token=${plants_token}&page_size=${data.page_size}&page=${data.page}&q=${data.q}&family${data?.family?.includes("family")?"_common":""}_name=${data.family}&complete_data=true`,
        { cancelToken: source.token },
      )
      .then((result => {
          setPlants(result.data);
          setLoading(false);
          setTotalPage(parseInt(result.headers["total-pages"]));            
        })
      )
      .catch(err => {
        console.error(err);
      });
    };

    if(Object.keys(queryString.parse(location.search)).length===0||(qs===""&&family==="")){
      setFamily('');
      setQS('');
      setPage(parseInt(''));
    }else {
      getPlantsList(parseInt(queryString.parse(location.search).page_no));
    }
    return () => {
      source.cancel();
    };
  }, [ location, family, qs ]);

  return (
    <Fragment>
      <Jumbotron
        title={family}
        images={`https://i.picsum.photos/id/798/4592/3448.jpg?hmac=a-NblRRC-lhb5GShHTdTomW3vlf5HZKM_aRjWQaOmNg`}
        show={!isNaN(page)}
      />
      <div className="jumbotron bg-light py-0 my-0">
        <div className="container px-0">
          <div className="row py-5 mr-auto ml-auto">
            <DropDownFilter
              location={location}
              history={history}
              setFamily={setFamily}
              family={family}
              qs={qs}
              className="col-lg-4 col-12"
            />
            <Pagination
              page={page}
              setPage={setPage}
              history={history}
              firstLastBtn={true}
              location={location}
              lastPage={totalPage}
              className="my-3 my-lg-0 col-lg-4 col-12 px-3"
            />
            <NameSearcher
              location={location}
              history={history}
              family={family}
              setQS={setQS}
              qs={qs}
              className="col-lg-4 col-12"
            />
          </div>
        </div>
      </div>
        {!isNaN(page) ? (
      <div className="container">
          <>
            <div className={"row"}>
              {page > 0 && plants?.length > 0 ? (
                plants.map((plant, index) => {
                  return (
                    <Card 
                      key={index} 
                      plant={plant}
                      data={{plants, page, pageSize, search:location.search, index}}
                      popTrigger={()=>{setShowModal(true);setIndexNow(index)}}
                    />
                  );
                })
              ) : loading ? (
                <div className="d-table col-12 text-center" style={{ height: "50vh" }}>
                  <div className="d-table-cell align-middle">
                    <Spinner animation="grow" />
                  </div>
                </div>
              )
                :
              <div className="container" style={{minHeight:'420px'}}>
                  <NoData/>
              </div>
            }
            </div>
            {plants && (
              <div className="d-flex justify-content-center my-5">
                <Pagination
                  page={page}
                  lastPage={totalPage}
                  setPage={setPage}
                  history={history}
                  location={location}
                  firstLastBtn={true}
                />
              </div>
            )}
          </>
      </div>
        )
        :
        <Explore/>
      }
      <Modal 
        show={showModal} 
        setShow={setShowModal} 
        dataList={plants} 
        modalContentComponent={PlantForModal} 
        indexNow={indexNow} 
        setIndexNow={setIndexNow}
        history={history}
        location={location}
      />
    </Fragment>
  );
};

export default Plants;