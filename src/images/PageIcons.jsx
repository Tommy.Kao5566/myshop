import React from "react";

export const toFirst = { viewBox:"0 0 24 24", icon:<path d="M18.41 16.59L13.82 12l4.59-4.59L17 6l-6 6 6 6zM6 6h2v12H6z"/>};

export const minus =  { viewBox:"0 0 24 24", icon: <path d="M19 13H5v-2h14v2z" />};

export const toPrevious = { viewBox:"0 0 24 24", icon: <path d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12l4.58-4.59z"/>};

export const plus = { viewBox:"0 0 24 24", icon:<path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z" />};

export const toNext = { viewBox:"0 0 24 24", icon:<path d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6-6-6z"/>};

export const toLast = { viewBox:"0 0 24 24", icon:<path d="M5.59 7.41L10.18 12l-4.59 4.59L7 18l6-6-6-6zM16 6h2v12h-2z" />};