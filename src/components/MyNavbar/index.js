import React from "react";
import Logo from "images/logo.svg";
import { Navbar, Nav } from "react-bootstrap";
import UserZoom from 'components/UserZoom'
import { Link } from "react-router-dom";

const MyNavbar = () => {

  return (
    <Navbar className="py-md-0 px-2" bg="light" variant="light" fixed="top" collapseOnSelect expand="sm">
      <Link className="px-2" to="/">
        <img src={Logo} alt="react-router-breadcrumb" height="30" className="ml-2 boomerang"/>
      </Link>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse>
        <Nav className="mr-auto mt-2">
          <Link to="/" className="h5 nav-link align-self-center">
            Home
          </Link>
          <Link to="/plants" className="h5 nav-link align-self-center">
            Plants
          </Link>
          <Link to="/taxonomy/families?page_no=1" className="h5 nav-link align-self-center">
            Families
          </Link>
        </Nav>
        <Nav className="ml-auto mt-2">
          <UserZoom/>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default MyNavbar;
