import React from "react";

const Jumbotron = ({ title, subtitle, images, show = true }) => {
  return (
    <div
      className={`jumbotron jumbotron-fluid my-0 text-capitalize${!show&&" hide"}`}
      style={{
        backgroundImage: `url(${images})`,
      }}
    >
      {show&&
        <div className="mx-5 px-3 px-md-4">
          <h1 className="display-4">{title}</h1>
          <p className="lead" style={{ color: "#777" }}>
            {subtitle}
          </p>
        </div>
      }
    </div>
  );
};

export default Jumbotron;
