import React from "react";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
import { Spring } from "react-spring/renderprops";

const ResultCard = ({ plant }) => {
  const { scientific_name, common_name, name } = plant;

  return (
    <Spring 
      from={{ marginTop: "-20px", opacity: 0 }} 
      to={{ marginTop: "0", opacity: 1 }} 
      config={{ tension: 80, friction: 15 }}
    >
      {props => <div className="col-lg-4 my-3">
        <div className="card" style={props}>
          <div className="card-body">
            <div>
              <span className="card-title">{scientific_name||name}</span>
            </div>
            <p className="card-text" style={{ textTransform: "capitalize" }}>
              { common_name || "NA"}
            </p>
            <Link to={`/plants?family_common_name=${common_name||""}&family_name=${name||""}&page_no=1`}>
              <Button variant="outline-success text-success">Explore</Button>
            </Link>
          </div>
        </div>
      </div>}
    </Spring>
  );
};

export default ResultCard;
