import React, { useState, useEffect } from "react";
import { Tab, Nav } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLemon, faSpa } from "@fortawesome/free-solid-svg-icons";
import { faEnvira } from '@fortawesome/free-brands-svg-icons'
import { handleColorString as handleColor } from 'utils/handleColorString'

import classnames from 'classnames';
import classes from './style.module.css'

const DataList = ({ data }) => {
  return (
    <>
      {data&&data.map((row) => {
          return (
            <tr key={row.dataKey}>
              <td width="50%" className="text-uppercase">
                {row.dataKey.replace(/_/g, " ")}
              </td>
              <td width="50%" className="text-capitalize">
                {row.dataInfo}
                {row.dataKey==="foliage_color"&&<span className="h5 ml-2"><FontAwesomeIcon icon={faEnvira} color={handleColor(row.dataInfo)}/></span>}
                {row.dataKey==="flower_color"&&<span className="h5 ml-2"><FontAwesomeIcon icon={faSpa} color={handleColor(row.dataInfo)} /></span>}
                {row.dataKey==="seed_color"&&<span className="h5 ml-2"><FontAwesomeIcon icon={faLemon} color={handleColor(row.dataInfo)} /></span>}
              </td>
            </tr>
          )
        })}
    </>
  );
};

const PlantInfo = ({ plant }) => {
  const [key, setKey] = useState(0);
  const [plantContents, setPlantContents] = useState([]);

  const tabFilter = (data) => {
    if (data) {
      const res = Object.keys(data).filter(key => {
        return data[key] !== null && typeof data[key] !== "object" && key!=='color' && data[key] !== "";
      }).map((dataKey)=>{
        if( data[dataKey]===false || data[dataKey]==="None" ){
          return { dataKey:dataKey , dataInfo:"No" }
        }else if(data[dataKey]===true){
          return { dataKey:dataKey , dataInfo:"Yes" }
        }
        return { dataKey:dataKey , dataInfo:data[dataKey] }
      });
      return res;
    }
    return [];
  };

  useEffect(() => {

    const { main_species } = plant;

    let propagationString = '';
    if (main_species?.propagation){
      Object.keys(main_species.propagation).forEach( way => {
        if (main_species.propagation[way] !== null) {
          if (propagationString?.length === 0){
            propagationString = way.replace(/_/g, " ");
          } else {
            propagationString = propagationString + ', ' + way.replace(/_/g, " ");
          }
        }
      })
    }

    const taxonomyItems = {
      species : plant?.main_species?.slug || 'NA' ,
      genus : plant?.genus?.name || 'NA' ,
      family : plant?.family?.name || plant?.family_common_name || 'NA' ,
      order : plant?.order?.name || 'NA' ,
      class : plant?.class?.name || 'NA' ,
      division : plant?.division?.name || 'NA' ,
    }

    const tabList = [
      { tabName : 'specifications', tabData: main_species?.specifications },
      { tabName : 'foliage', 
        tabData: 
        { duration:plant.duration,
          ...main_species?.foliage,
          foliage_color:main_species?.foliage.color||"", 
          ...main_species?.flower, 
          flower_color: main_species?.flower.color||""
        } 
      },
      { tabName : 'seed', 
        tabData: 
        { ...main_species?.seed,
          ...main_species?.fruit_or_seed, 
          seed_color: main_species?.fruit_or_seed.color||"",
          propagation:propagationString
        }
      },
      { tabName : 'growth', tabData: main_species?.growth },
      { tabName : 'taxonomy', tabData: taxonomyItems },
    ]

    const tempArr = [];

    tabList.forEach((tab)=>{
      let tempData = tabFilter(tab.tabData);
      if (tempData.length>0){
        tempArr.push({ name: tab.tabName, item: tempData });
      }
    })
    setPlantContents(tempArr);
  }, [plant])

  return (
    <Tab.Container activeKey={key} onSelect={k => setKey(k)}>
      {plantContents && 
      <div className="row">
        <div className="border-right px-0 col-1">
          <Nav variant="pills" justify="true" className={"flex-column"}>
            {plantContents.map((content, index) => {
              return (
                <Nav.Item key={`${content.name}_${index}`} style={{zIndex:index}} className={classes.tabAnimation2}>
                  <Nav.Link eventKey={index} className={classnames(classes.myTabs, "px-0 py-3")}>
                    <span className={classes.verticalText}>{content.name}</span>
                  </Nav.Link>
                </Nav.Item>
              );
            })}
          </Nav>
        </div>
        <div className="col-11">
          <Tab.Content>
            {plantContents.map((content, index) => {
              return (
                <Tab.Pane eventKey={index} key={`${content.name}_${index}`}>
                  <table className="table table-hover" style={{ fontSize: 14 }}>
                    <tbody>
                      <DataList data={content.item} />
                    </tbody> 
                  </table>
                </Tab.Pane>
              )
            })}
          </Tab.Content>
        </div>
      </div>}
    </Tab.Container>
  );
};

export default PlantInfo;
