import React from "react";

const SquareFilter = ({children})=>{

    return (
      <div style={{ width:"100%", paddingTop:'100%', position:'relative', overflow:'hidden'}}>
        <span style={{ position: 'absolute', top: '50%',  left: '50%', transform: 'translate(-50%, -50%)', overflow:'hidden', minWidth:'100%', minHeight:'100%'}}>
          {children}
        </span>
      </div>
    )
}

export default SquareFilter;