import React, { useState, useEffect } from "react";
import CenterItem from "components/CenterItem";
import { Spinner, Popover } from "react-bootstrap";
import { apiEndpoint, plants_token } from "apis/trefle";
import axios from "axios";
import { LazyLoadImage } from "react-lazy-load-image-component";
import "react-lazy-load-image-component/src/effects/blur.css";

const ImgPopover = ({ plant_id }) => {
    const [loading, setLoading] = useState(false);
    const [imgs, setImgs] = useState(null);
    const source = axios.CancelToken.source();
  
    useEffect(() => {
      const getPlantsImg = plant_id => {
        setLoading(true);
  
        axios
          .all([
            axios.get(`${apiEndpoint}/api/plants/${plant_id}?token=${plants_token}`, {
              cancelToken: source.token
            })
          ])
          .then(
            axios.spread(plants => {
              setImgs(plants?.data?.images);
              setLoading(false);
            })
          )
          .catch(err => {
            if (axios.isCancel(err)) {
              // request cancelled
            } else {
              console.error(err);
              throw err;
            }
          });
      };
  
      getPlantsImg(plant_id);
  
      return () => {
        source.cancel();
      };
    }, [plant_id]);
  
    return (
      <>
        <Popover.Content>
          <div>
            {imgs?.length > 0 ? (
              <>
                <LazyLoadImage
                  beforeLoad={() => setLoading(true)}
                  afterLoad={() => setLoading(false)}
                  src={imgs[0].url}
                  effect="blur"
                  style={{ objectFit: "cover", width: 150, height: 150 }}
                />
                {loading && (
                  <CenterItem>
                    <Spinner animation="grow" />
                  </CenterItem>
                )}
              </>
            ) : loading ? (
              <Spinner animation="grow" />
            ) : (
              <div
                style={{
                  width: 150,
                  height: 150,
                  backgroundColor: "#eeeeee",
                  display: "flex",
                  justifyContent: "center"
                }}
              >
                <span style={{ alignSelf: "center", textAlign: "center" }}>
                  <p style={{ fontSize: 24, margin: 0 }}>!</p>
                  No Photo
                </span>
              </div>
            )}
          </div>
        </Popover.Content>
      </>
    );
  };

export default ImgPopover;