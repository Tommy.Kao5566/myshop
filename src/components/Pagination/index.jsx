import React from "react";
import {toFirst, toLast, plus, minus }  from "images/PageIcons";

function Pagination({ page, setPage, history, location, firstLastBtn =false, lastPage=200000, className}) {

  const MySvg = ({path})=>{
    return (
      <svg width="24px" height="24px" viewBox={path?.viewBox} fill='#777'>
        {path.icon}
      </svg>
    )
  }

  const handleMinus = ( page = 1 ) => {
    let searchParams = new URLSearchParams(location.search);
    if (page > 1) {
      page --;
    }
    setPage(page);
    searchParams.set('page_no', page);
    history.push({
      pathname: location.pathname,
      search: searchParams.toString()
    });
  };

  const handlePlus = ( page = lastPage ) => {
    let searchParams = new URLSearchParams(location.search);
    if (page < lastPage) {
      page++;
    } 
    setPage(page);
    searchParams.set('page_no', page);
    history.push({
      pathname: location.pathname,
      search: searchParams.toString()
    });
  };

  return (
    <div id="pagination" className={`btn-group ${className}`} role="group" aria-label="Basic example">
      {firstLastBtn&&
        <div className={`btn btn-outline-secondary border-0 rounded-0${page===1||isNaN(page)?" disabled":""}`}
            onClick={() => { handleMinus(); }} 
        >
          <MySvg path={toFirst}/>
        </div>
      }
      <div className={`btn btn-outline-secondary border-0 rounded-0${page===1||isNaN(page)?" disabled":""}`}
          onClick={() => { handleMinus(page); }} 
      >
          <MySvg path={minus}/>
      </div>
      <div className="btn btn-outline-secondary border-0 rounded-0 fakeBtn w-auto"
        style={{ minWidth:'55px' }}
      >
        {!isNaN(page)&&page}
      </div>
      <div className={`btn btn-outline-secondary border-0 rounded-0${page===lastPage||isNaN(page)?" disabled":""}`}
        onClick={() => { handlePlus(page);}} 
      >
          <MySvg path={plus}/>
      </div>
      {firstLastBtn&&
        <div className={`btn btn-outline-secondary border-0 rounded-0${page===lastPage||isNaN(page)?" disabled":""}`}
          onClick={() => { handlePlus();}} 
        >
          <MySvg path={toLast}/>
        </div>
      }
    </div>
  );
}

export default Pagination;