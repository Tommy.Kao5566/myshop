import React, { useState, useEffect, useRef } from "react";
import { toPrevious, toNext } from "images/PageIcons";
import classes from "./style.module.css";

const getWidth = () => window.innerWidth;

const NavControler = ({ direction, handleClick }) => {
  const navPosition = {
    right: direction === 'right' && '25px',
    left: direction === 'left' && '25px',
  }

  return (
    <div style={navPosition} className={classes.navControler} onClick={handleClick}>
      {direction === 'right' 
        ? <svg width="45" viewBox={toNext.viewBox}>
          {toNext.icon}
        </svg> 
        : <svg width="45" viewBox={toPrevious.viewBox}>
          {toPrevious.icon}
        </svg>
        }
    </div>
  )
}

const Indicator = ({ active }) => {
  return (
    <span style={{ opacity: active ? 1 : 0.5 }} className={classes.indicator} />
  )
}

const Indicators = ({ activeIndex, contents }) => {
  return (
    <div className={classes.indicators}>
      {contents.map((content, index) => {
        return (<Indicator key={content} active={activeIndex === index} />);
        })
      }
    </div>
  )
}

const Container  = ({ translate, transition, width, children }) => {
  const containerStyle = {
    transform: `translateX(-${translate}px)`,
    transition: `transform ease-out ${transition}s`,
    width: `${width}px`,
    height: '100%',
    display: 'flex'
  }
  return (
    <div className="SlideContainer" style={containerStyle}>{children}</div>
  )
}

const Content = ({ content, width }) => {
  const contentStyle = { 
    height: '100vh', 
    width: `${width}px`, 
    backgroundImage: `url(${content})`,
  }
  return (
    <div style={contentStyle} className={classes.content} />
  );
}

const Carousel = ({ contents, autoPlay }) => {  
  const contentCount = contents.length;
  const firstContent = contents[0];
  const secondContent = contents[1];
  const lastContent = contents[contentCount - 1];
  const [_images, setImages] = useState([lastContent, firstContent, secondContent]);

  const [activeIndex, setActiveIndex] = useState(0);
  const [translate, setTranslate] = useState(getWidth());
  const [transition, setTransition] = useState(0.45);
  const [btnLock, setBtnLock] = useState(false);

  const autoPlayRef = useRef();
  const transitionRef = useRef();
  const resizeRef = useRef();

  useEffect(() => {
    autoPlayRef.current = handleNext;
    transitionRef.current = smoothTransition;
    resizeRef.current = handleResize;
  })

  useEffect(() => {
    const play = () => { autoPlayRef.current() };

    const smooth = e => { 
      if (e.target.className === 'SlideContainer') {
          transitionRef.current();
        }
      };
    const resize = () => { resizeRef.current() };
    
    const transitionEnd = window.addEventListener('transitionend', smooth);
    const onResize = window.addEventListener('resize', resize);

    let interval = null;
    if (autoPlay) {
      interval = setInterval(play, autoPlay * 1000);
    };

    return () => {
      window.removeEventListener('transitionend', transitionEnd);
      window.removeEventListener('resize', onResize);
      if (autoPlay) { clearInterval(interval); }
    };
  }, [autoPlay])

  useEffect(() => {
    if (transition === 0) { setTransition(0.45) }
  }, [transition]);
  
  const handleResize = () => {
    setTranslate(getWidth());
    setTransition(0);
  }

  const smoothTransition = () => {
    let _images = [];

    if (activeIndex === contentCount - 1) {
      _images = [contents[contentCount - 2], lastContent, firstContent];
    } else if (activeIndex === 0) {
      _images = [lastContent, firstContent, secondContent];
    } else {
      _images = contents.slice(activeIndex - 1, activeIndex + 2);
    }
    setImages(_images);
    setTranslate(getWidth());
    setTransition(0);
  }

  const handleNext = () => {
    if (btnLock) { return null; };
    setBtnLock(true);
    setTimeout(() => setBtnLock(false),500);

    setTranslate(translate + getWidth());
    if (activeIndex === contentCount - 1) {
      setActiveIndex(0);
    } else {
      setActiveIndex(activeIndex + 1);
    }
  }
  
  const handlePrevious = () => {
    if (btnLock) { return null; };
    setBtnLock(true);
    setTimeout(() => setBtnLock(false),500);

    setTranslate(0);
    if (activeIndex === 0) {
      setActiveIndex(contentCount - 1);
    } else {
      setActiveIndex(activeIndex - 1);
    }
  }

  return (
    <div className={style.carousel} autoPlay={autoPlay}>
      <Container
        translate={translate} 
        transition={transition}  
        width={getWidth() * _images.length}
      >
        {_images.map((_image, index) => {
          return (
            <Content width={getWidth()} key={_image + index} content={_image} />
          );
        })}
      </Container>
      <NavControler direction='left' handleClick={handlePrevious} />
      <NavControler direction='right' handleClick={handleNext} />
      <Indicators contents={contents} activeIndex={activeIndex} />
    </div>
  );
};

export default Carousel;
