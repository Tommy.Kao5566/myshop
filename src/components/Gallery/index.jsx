import React, { useState } from 'react';
import classnames from 'classnames'
import classes from './style.module.css';

const Image = ({ src, classname }) => {

  const [loaded, setLoaded] = useState(false);

  return (
    <img src={src}
      className={classnames(classname, !loaded?classes.hide:null)}
      onLoad={()=>{setLoaded(true);}}
      alt=""
    />
  )
}

const Gallery = ({ images }) => {
  
  const [activeIndex, setActiveIndex] = useState(0);
  
  return ( 
    <div className={classes.galleryContainer}>
      <Image src={images[activeIndex]?.url} classname={classes.mainImg}/>
      {images.length > 1 && images.map((img,idx) => {
        return (
          idx < 8 &&
            <div key={'img_'+idx} className={classes.smallImgContainer} onClick={()=>{setActiveIndex(idx);}}>
              <Image src={img.url} classname={classes.smallImg}/>
            </div>
        )
      })}
    </div>
  );
}

export default Gallery;