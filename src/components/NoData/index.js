import React from "react";
import NoDataImg from 'images/NoData.jpg';

const NoData =()=>{

    return (
        <div className="d-block w-100" 
            style={{ 
                height:'380px', 
                backgroundImage:`url(${NoDataImg})`, 
                backgroundRepeat: 'no-repeat', 
                backgroundPosition:'-128px -225px',
                paddingLeft:'350px',
                boxShadow: '0 0 8px 8px white inset'
            }}>
            <div className="h1" style={{margin:'40px 0 20px 0'}}> No Result ... </div>
            <p className="font-italic">"Every withered flower had blossomed at least once."</p>
        </div>
    )
}

export default NoData;