import React, { useEffect, useState } from "react";

const Explore =()=>{

    const [pt, setPt]=useState(0)

    const handleScroll =(e)=>{
        setPt(e.target.scrollingElement.scrollTop);
    }

    useEffect(()=>{
        window.addEventListener('scroll', handleScroll);
        return()=>{
            window.removeEventListener('scroll', handleScroll);
        }
    },[])

    return(
        <div className="text-light text-center font-weight-light"
            style={{
                height:'80vh',
                width:'100%',
                backgroundSize:'cover',
                backgroundRepeat: 'no-repeat',
                backgroundImage:'url("https://bloximages.chicago2.vip.townnews.com/rapidcityjournal.com/content/tncms/assets/v3/editorial/d/8f/d8f2abab-dc04-5257-9d46-c14484f9f230/5b842567b57ac.image.jpg?resize=1700%2C1133")'
            }}
        >
            <h1 className="font-weight-normal" style={{paddingTop:`${(pt+600)/3}px`}}>
                Explore more than 164,988 plants...
            </h1>
            <h5 className="font-weight-light mt-4">Data powered by Trefle API</h5>
        </div>
    )
}

export default Explore;