import React from "react";

const CenterItem = ({ children })=>{

    return (
        <span style={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)'
        }}>
            {children}
        </span>
    )
}

export default CenterItem;