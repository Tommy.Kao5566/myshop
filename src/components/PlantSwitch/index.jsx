import React from "react";
import { Link } from "react-router-dom";
// import axios from "axios";
// import { plants_token } from "apis/trefle";
// import queryString from "query-string";
import { toPrevious, toNext } from "images/PageIcons";

const PlantSwitch = ({ location }) => {
 
  const { data } = location;
  const path = location.pathname.substring(0,location.pathname.lastIndexOf('/'));

  return (
    <div className="p-3" style={{marginTop:'-156px',marginBottom:'84px'}}>
      <div className="justify-content-between row m-0">
        <Link to={{pathname: path +'/'+ data.plants[data.index-1]?.id, data:{...data, index:data.index-1}}}>
          <div className="pointerIcon">
            <svg width="40" height="40" viewBox={toPrevious.viewBox} fill="#c1c4c7">
              {toPrevious.icon}
            </svg>
          </div>
        </Link>
        <Link to={{pathname: path +'/'+ data.plants[data.index+1]?.id, data:{...data, index:data.index+1}}}>
          <div className="pointerIcon">
            <svg width="40" height="40" viewBox={toNext.viewBox} fill="#c1c4c7">
              {toNext.icon}
            </svg>
          </div>
        </Link>
      </div>
    </div>
  );
};
 
export default PlantSwitch;
 