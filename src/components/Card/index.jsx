import React from "react";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
// import { OverlayTrigger, Popover, Button } from "react-bootstrap";
import { Spring } from "react-spring/renderprops";
// import ImgPopover from "components/ImgPopover";

const Card = ({ plant, popTrigger, data }) => {
  const { scientific_name, common_name, id } = plant;

  return (
    <Spring 
      from={{ marginTop: "-20px", opacity: 0 }}
      to={{ marginTop: "0", opacity: 1 }}
      config={{ tension: 80, friction: 15 }}
    >
      {props => <div className="col-lg-4 my-3">
        <div className="card" style={props}>
          <div className="card-body">
            {/* <OverlayTrigger
              placement={"right"}
              overlay={
                <Popover>
                  <ImgPopover plant_id={id} />
                </Popover>
              }
            > */}
              <span className="card-title text-capitalize" onClick={()=>{popTrigger(true)}}>{common_name || scientific_name}</span>
            {/* </OverlayTrigger> */}
            <p className="card-text text-capitalize">
              {common_name && scientific_name}
            </p>
              <Link to={{ pathname: `/plants/${id}`, data}}>
                <Button variant="outline-success text-success">More info</Button>
              </Link>
          </div>
        </div>
      </div>}
    </Spring>
  );
};

export default Card;
