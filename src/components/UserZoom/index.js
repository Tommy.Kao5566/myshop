import React, { useState, useEffect, useContext } from "react";
import { AuthStore } from 'contexts/AuthContext'

const UserZoom = ()=>{

    const authData = React.useContext(AuthStore);
    
    const { isLogin, login, logout } = authData;

    return (
        <div>
            {!isLogin?
                <div className="btn" onClick={()=>{login()}}>Login</div>
            :
                <div className="btn" onClick={()=>{logout()}}>Logout</div>
            }
        </div>
    )
}

export default UserZoom;