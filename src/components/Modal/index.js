import React, { useState, useEffect, createElement } from "react";
import { Link } from "react-router-dom";
import { toPrevious, toNext } from "images/PageIcons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClone, faHeart as fasHeart, faTimes, faBookOpen } from "@fortawesome/free-solid-svg-icons";
import { faHeart as farHeart } from "@fortawesome/free-regular-svg-icons";
import classnames from 'classnames';
import classes from './style.module.css';

const ModalBlock = ({ handleClose, indexNow, modalContentComponent, data, order })=>{

    const title = data.common_name ?
                    <> {data.common_name} <span className="d-none d-lg-inline-block">{` | ${data.scientific_name}`}</span></>
                    :
                    data.scientific_name

    const modalContent = createElement(modalContentComponent, {
        plant_id: data.id,
    })

    const [loaded, setLoaded] = useState(false);

    useEffect(()=>{
        if(order===indexNow){
            setLoaded(true);
        }
    },[indexNow, order])

    return (
        <div className={classes.modalBlock}>
            <div className="modal-content">
                <div className="modal-header bg-dark">
                    <h3 className="modal-title text-capitalize ml-4 text-light">
                        {title}
                    </h3>
                    <div className={classnames(classes.otherBtns, "d-none d-sm-inline-block")}>
                        <div className="close">
                            <FontAwesomeIcon icon={faClone}/>
                        </div>
                        <Link to={`/plants/${data.id}`}>
                            <div className="close">
                                <FontAwesomeIcon icon={faBookOpen}/>
                            </div>
                        </Link>
                        <div className="close">
                            <FontAwesomeIcon icon={farHeart || fasHeart}/>
                        </div>
                    </div>
                    <div className="close" onClick={handleClose}>
                        <FontAwesomeIcon icon={faTimes}/>
                    </div>
                </div>
                <div className={classnames(classes.modalBody, "modal-body px-0 px-sm-3")}>
                    {loaded && modalContent}
                    <div className={classnames(classes.BtnsForMobile, "d-flex d-sm-none")}>
                        <div className="close bigBtn">
                            <FontAwesomeIcon icon={farHeart || fasHeart}/>
                        </div>
                        <Link to={`/plants/${data.id}`}>
                            <div className="close bigBtn">
                                <FontAwesomeIcon icon={faBookOpen}/>
                            </div>
                        </Link>
                        <div className="close bigBtn">
                            <FontAwesomeIcon icon={faClone}/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const BtnForModal = ({ indexNow, setIndexNow, lastIndex}) =>{

    return (
        <div className={classnames(classes.btnGroupForModal, "d-none d-sm-inline-block")}>
            <div onClick={()=>{
                if(indexNow>0){
                    setIndexNow(indexNow-1);
                }
            }}>
                <svg width="100%" viewBox={toPrevious.viewBox} fill="#fff">
                    {toPrevious.icon}
                </svg>
            </div>
            <div onClick={()=>{
                if(indexNow < lastIndex ){
                    setIndexNow(indexNow+1);
                }
            }}>
                <svg width="100%" viewBox={toNext.viewBox} fill="#fff">
                    {toNext.icon}
                </svg>
            </div>
        </div>
    )
}

const Modal = ({ modalContentComponent, show, setShow, dataList, indexNow, setIndexNow }) => {

    const [motion1, setMotion1] = useState(false);
    const [motion2, setMotion2] = useState(false);

    const handleClose =()=>{
        setShow(false);
    }

    useEffect(()=>{

        if(show){
            setMotion1(true);
            setTimeout(()=>{setMotion2(true)},100)
        }else{
            setMotion2(false);
            setTimeout(()=>{
                setMotion1(false);
            },100)
        }

    },[show])

    return(
        <div className={`modal overflow-hidden fade in${motion1?" d-block":" d-none"} ${motion2?"show":""}`}>
            <div className="modal-backdrop fade in show" onClick={()=>{handleClose()}}/>
            {dataList && 
                <div className="position-relative text-nowrap" style={{transition: 'left 0.5s', left:`${-indexNow}00%`}}>
                    {dataList.map(( data, index)=>{
                        return (
                            <ModalBlock
                                indexNow={indexNow}
                                order={index}
                                key={'ModalBlock'+index} 
                                handleClose={handleClose}
                                modalContentComponent={modalContentComponent} 
                                data={data}
                            />
                        )
                    })}
                </div>
            }
            {dataList?.length > 1 && <BtnForModal indexNow={indexNow} setIndexNow={setIndexNow} lastIndex={dataList.length-1}/>}
        </div>
    );
}
export default Modal;