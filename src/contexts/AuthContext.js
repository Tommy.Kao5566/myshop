import React, { createContext, useState, useEffect } from 'react';
import Cookies from 'js-cookie';
import axios from "axios";

import { postAuthLoginAPI, getAuthAccountAPI } from 'apis/auth.js'

export const AuthStore = createContext();

export const withAuthProvider = (WrappedComponent) => {

    const source = axios.CancelToken.source();

    const AuthProvider = (props) => {

        const [isLogin, setIsLogin] = useState(false);

        const login = ( username = 'jacky', password = 'jacky', is_remember_me = false )=>{
            
            const data = {
                username, password, grant_type: "password"
            };
            setIsLogin(true);

            axios.post(
                postAuthLoginAPI(), data,
                {   
                    cancelToken: source.token,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                    },
                }
            )
            .then((result => {

                console.log(result)
                setIsLogin(true);
                
                if (is_remember_me) {
                    Cookies.set('access_token', 'access_token', { expires: 7 });
                    Cookies.set('is_remember_me', is_remember_me, { expires: 7 });
                } else {
                    Cookies.set('access_token', 'access_token');
                }

            }))
            .catch(err => {
                console.error(err);
            });
        }

        const logout=()=>{
            setIsLogin(false);
            Cookies.remove('access_token');
            Cookies.remove('is_remember_me');
        }

        useEffect(() => {
            const token = Cookies.get('access_token');

            if(token){
                setIsLogin(true);
            }
        }, [])

        const authData = {
            isLogin,
            logout,
            login
        };

        return (
            <AuthStore.Provider value={authData}>
                <WrappedComponent {...props} />
            </AuthStore.Provider>
        );
    }

    return AuthProvider;
}