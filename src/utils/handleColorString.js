export const handleColorString = (color)=>{

    color = color.toLowerCase().match(/\w/g).join('');

    switch (color) {
      case !color:
        color = null;
        break;
      case "whitegray":
        color = "#D3D3D3";
        break;
      case "graygreen":
        color = "#AFB7AD";
        break;
      case "white":
        color = "#e6e6e6";
        break;
      default:
        break;
    }
    return color;
}