const proxy = require('http-proxy-middleware');

module.exports =(app)=>{
    app.use(
        proxy('/oauth2',{
            target:'http://localhost:8080',
            changeOrigin:true
        })
    ).use(
        proxy('/api',{
            target:'https://trefle.io',
            changeOrigin:true
        })
    )
}